<?php 

use Filter\Vigenere;
use Filter\Database;


require 'Filter/Vigenere.php';
require 'Filter/Database.php';

$date = new \DateTime("now", new \DateTimeZone("GMT"));

if (isset($_POST['donnee'])){

    $data = $_POST['donnee'];

    $authDevice = new Database();
    $vige = new Vigenere($date->format("B"),$date->format("Y"),$date->format("i"));

    $retourAppli = $vige->vigenere($data, true);
  
    $JsonRetourAppli = json_decode($retourAppli);  

    $isAuth = $authDevice->authTokenCheck($JsonRetourAppli->body->nom);

    if($isAuth != FALSE){

        $url = curl_init('https://mementumapi.000webhostapp.com/public/index.php'); //adresse api
        curl_setopt($url, CURLOPT_POST, true);
        curl_setopt($url, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($url, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($url, CURLOPT_POSTFIELDS, array('donnee' => $retourAppli));
        
        
        $response = curl_exec($url);
        $state = curl_getinfo($url);
        
        if ($state['http_code'] !== 200) {
 
            echo "Une erreur est survenu veuillez réessayer";

        }else {
        
            echo($response);
        }
    }else{

        echo "Vous n'êtes pas autorisés à accéder à cette API";
    }
}