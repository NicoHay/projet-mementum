<?php

namespace Filter;

/**
 *  Vigenere encryption algorithm
 * 
 * @author  NicoHay
 *  
 */

class Vigenere
{

    private $key;

    //lenght 80 characters
    private $chars = array(
        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j',
        'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't',
        'u', 'v', 'w', 'x', 'y', 'z', '1', '2', '3', '4',
        '5', '6', '7', '8', '9', '0', 'A', 'B', 'C', 'D',
        'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N',
        'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
        'Y', 'Z', ':', ';', '!', ',', '.', '@', '"', '[',
        ']', '{', '}', '(', ')', '?', '-', '=', '/', '_',
    );
    /**
     * Build key in constructor
     * 
     * @param String $key encryption key
     */
    public function __construct($nombre1, $nombre2, $nombre3)
    {
        
        
        $this->key = str_split($this->keyGenerator($nombre1, $nombre2, $nombre3));
        
    }
    /**
     ********************************************************************* 
     * Encryption formula
     * E = (p + k) mod 80
     * 
     * @param  Integer $k index of key text
     * @param  Integer $p index of plain text
     * 
     * @return Integer
     ********************************************************************* 
     */
    private function encrypt($p, $k)
    {


        $sum = ($p + $k) % 80;

        if ($sum >= 80) {
            $sum = $sum - 80;
        }
        return $sum;

    }

    /**
     ********************************************************************* 
     * Decryption formula
     * D = (p -k) 
     * 
     * @param  Integer $k index of key text
     * @param  Integer $p index of plain text
     * 
     * @return Integer
     * 
     ********************************************************************* 
     */
    private function decrypt($p, $k)
    {


        $sum = ($p - $k);

        if ($sum < 0) {
            $sum = $sum + 80;
        }
        return $sum;


    }

    
    /**
     *
     * @param int $nombre1
     * @param int $nombre2
     * @param int $nombre3
     *
     * @return string
     */
    private function keyGenerator($nombre1,$nombre2,$nombre3)
    {
        
        $date = new \DateTime("now", new \DateTimeZone("GMT"));
        $dateString = $date->format("YHi");
        
        $clef = '';
        $forAscii ='';
        $ascii= "";
        
        for ($i = 0; $i < strlen($dateString); $i++) {
            
            $clef .= str_replace('.', '', ($dateString[$i] * $nombre1) + (sqrt($nombre2)) * (($nombre1 + $nombre3) % $nombre2));
            
        }
        
        for ($i = 0; $i < strlen($clef); $i++) {
            
            $forAscii       =  chr(intval(substr($clef, $i , 2) . ' '));
            
            if(ctype_alnum($forAscii)){
                $ascii          .= $forAscii;
            }
            
        }
        //echo('<br> la date => '.$dateString .' <br> la clef => '.$clef .' <br> la clef secrete => '.$ascii);
        return $ascii;
    }
    

    /**
     ********************************************************************* 
     * Encryption or decryption process
     *
     * @param  String $plaintext plain text
     * @param  Bool is decrypt
     * 
     * @return String
     * 
     *********************************************************************      */

    public function vigenere($plaintext, $decrypt = false)
    {

        $explodeText = str_split($plaintext);
        $explodeKey = array();
        $index = 0;
        $result = '';


		// build key array
        for ($i = 0; $i < count($explodeText); $i++) {
            if ($index === count($this->key)) {
                $index = 0;
            }
            $explodeKey[] = $this->key[$index];

            $index++;
        }


        for ($i = 0; $i < count($explodeText); $i++) {


            $p = array_search($explodeText[$i], $this->chars);


            $k = array_search($explodeKey[$i], $this->chars);

            if ($decrypt) {
                $codage = $this->decrypt($p, $k);
            } else {
                $codage = $this->encrypt($p, $k);
            }

            $result .= $this->chars[$codage];

        }
        return $result;
    }


}
