<?php
namespace Filter;
use PDO;
/**
 *
 * @author nicohay
 *        
 */
class Database
{

   
    public $pdo;
    public $user;
    
    
    
    /**
     * Check if the user have a device token 
     *
     * @param string $user
     *
     * @return MIXED
     */

    public function authTokenCheck(string $userlogin){
        
        // si aucun objet PDO existe on creer une connection
        if(!$this->pdo){
            
            $this->pdo = new PDO("mysql:dbname=id8817221_authmementum;host=localhost",'id8817221_nico','coda2018');
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
            $this->pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE,PDO::FETCH_OBJ);
           
        } 

        $req = $this->pdo->prepare("SELECT `device_token` FROM auth WHERE nom = :user ");
        $req->bindParam(':user', $userlogin);
        $req->execute();

        $this->user = $req->fetch();
        
        
        return $this->user;

        }
     
        
 
        
        
        
    }
