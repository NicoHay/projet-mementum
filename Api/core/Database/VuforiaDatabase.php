<?php
namespace core\Database;

use core\Config;



/**
 *
 * @author nicohay
 *        
 */
class VuforiaDatabase extends Database
{
    private $access_key;
    private $secret_key;

    const BASE_URL          = 'https://vws.vuforia.com';
    const JSON_CONTENT_TYPE = 'application/json';
    const TARGET_PATH       = '/targets';
    
    public $imagePath       = '../public/images/qr/';
    
    
    public function __construct(){
        
        $config = Config::getInstance(ROOT . '/config/config.php');
        
        $this-> access_key = $config->get('access_key');
        $this-> secret_key = $config->get('secret_key');
    }
    
    
    
    public function addTarget( $imageName , $data ){
        
        $imagePath  = $this->imagePath ;
        $metadata   = base64_encode( json_encode( $data ));
      
        $url        = curl_init( self::BASE_URL. self::TARGET_PATH );        
        curl_setopt( $url, CURLOPT_POST, true );
        curl_setopt( $url, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $url, CURLOPT_SSL_VERIFYPEER, false );
        
        $image = file_get_contents( $imagePath . $imageName );
       
        $image_base64   = base64_encode($image);
        
        $post_data      = array( 
            
            'name'                  =>  $imageName ,
            'width'                 =>  32.0 ,
            'image'                 =>  $image_base64 ,
            'application_metadata'  =>  $metadata ,
            'active_flag'           => 1          
            
        );
        
        $body = json_encode($post_data);
    
        
        curl_setopt($url, CURLOPT_HTTPHEADER, $this->getHeaders('POST', self::TARGET_PATH, self::JSON_CONTENT_TYPE, $body));
        curl_setopt($url, CURLOPT_POSTFIELDS, $body);
        
        $response   = curl_exec($url);
        $state      = curl_getinfo($url);
        
        if ($state['http_code'] !== 201) {
            
           return FALSE;

        } else {
            
          echo($response);
          
            return $response;            
        }
        
    }
    
    
    /**
     * *****************************************************************
     * 
     * supprime une target  de la bdd vuforia
     * 
     * @param STRING vuforiaTargetID - id de la target dans la bdd vuforia
     * 
     * @return BOOLEAN  
     * 
     * ***************************************************************** 
     */
    public function deleteTarget($targetID) {
        
        $path   = self::TARGET_PATH . "/" . $targetID;
        
        $url    = curl_init(self::BASE_URL . $path);
        
        curl_setopt($url, CURLOPT_CUSTOMREQUEST, 'DELETE');
        curl_setopt($url, CURLOPT_HTTPHEADER, $this->getHeaders('DELETE', $path));
        curl_setopt($url, CURLOPT_RETURNTRANSFER, true);
        
        $response   = curl_exec($url);
        $info       = curl_getinfo($url);
        
        if ($info['http_code'] !== 200) {
  
           return FALSE;
        }

        return TRUE;
    }
    
     /**
     * *****************************************************************
     * 
     * retourne le contenu d'une target .
     * 
     * @param STRING vuforiaTargetID - id de la target dans la bdd vuforia
     * 
     * @return [String]  Target Record
     * 
     * ***************************************************************** 
     */
     public function getTarget($targetID) {
         
        $url = curl_init(self::BASE_URL . self::TARGET_PATH. "/" .$targetID);
        curl_setopt($url, CURLOPT_HTTPHEADER, $this->getHeaders('GET', $path = self::TARGET_PATH. '/' .$targetID, $content_type = '', $body = ''));
        curl_setopt($url, CURLOPT_RETURNTRANSFER, true);
        $response   = curl_exec($url);
        $info       = curl_getinfo($url);
        if ($info['http_code'] !== 200) {

            return FALSE;
        }
        
        
        return json_decode($response);
    }



    
    
    private function getHeaders($method , $path = self::TARGET_PATH , $content_type = '', $body= ''){
      
        
        $headers = array();
        
        $date       = new \DateTime("now", new \DateTimeZone("GMT"));
        $dateString = $date->format("D, d M Y H:i:s") . " GMT";
        $md5        = md5($body, false);
        
        $string_to_sign = $method . "\n" . $md5 . "\n" . $content_type . "\n" . $dateString . "\n" . $path;
        
        $signature = $this->hexToBase64(hash_hmac("sha1", $string_to_sign , $this->secret_key));
        
        $headers[] = 'Authorization: VWS ' . $this->access_key . ':' . $signature;
        $headers[] = 'Content-Type: ' . $content_type;
        $headers[] = 'Date: ' . $dateString;
        return $headers;  
    }

    /**
     * *****************************************************************
     * 
     * retourne les target d'un user en particulier  .
     * 
     * @param STRING vuforiaTargetID - id de la target dans la bdd vuforia
     * 
     * @return [String]  Target Record
     * 
     * ***************************************************************** 
     */
    public function getUserTargets($targetID) {
         
        $url = curl_init(self::BASE_URL . self::TARGET_PATH. "/" .$targetID);
        curl_setopt($url, CURLOPT_HTTPHEADER, $this->getHeaders('GET', $path = self::TARGET_PATH. '/' .$targetID, $content_type = '', $body = ''));
        curl_setopt($url, CURLOPT_RETURNTRANSFER, true);
        $response   = curl_exec($url);
        $info       = curl_getinfo($url);
        if ($info['http_code'] !== 200) {

            return FALSE;
        }
        
        
        return json_decode($response);
    }

       
    private function hexToBase64($hex){
        $return = "";
        foreach(str_split($hex, 2) as $pair){
            $return .= chr(hexdec($pair));
        }
        return base64_encode($return);
    }
    
    
}