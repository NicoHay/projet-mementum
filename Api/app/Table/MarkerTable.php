<?php
namespace app\Table;

use core\Table\Table;
/**
 *
 * @author nicohay
 *        
 */
class MarkerTable extends Table{
    
    
    protected $table = "markers";
  
    
    
    public function deleteByIdInfo($id) {
        
        return $this->query("DELETE FROM {$this->table} WHERE id_info = ?", [$id], true);
    }
}

