<?php
namespace Api\app\Table;

use core\Table\Table;

/**
 *
 * @author nicohay
 *        
 */
class AuthTable extends Table
{
    protected $table = "Auth";

    /**
     */

    public function sessionLaps($id , $sessionToken, $hour){
        
            return $this->query("SELECT *
                            FROM {$this->table} WHERE id_user = ?
                            AND session_token = ?
                            AND NOW() < session_time + INTERVAL ? HOUR", [$id,$sessionToken,$hour], false);          
    }
    
   
}

