<?php 
namespace app\Controller;


/**
 * @author nicohay
 *        
 */

class MarkersController extends AppController
{

    public function __construct()
    {
        parent::__construct();       

        $this->loadModel('Marker');
        
    }
    
    /**
     *********************************************************
     * 
     * recupère les markers d'un User en particulier 
     * 
     * @param int $user
     * 
     * @return VOID
     *  
     *******************************************************
     */
    public function markers($user){
        
       
            
            $posts = $this->Marker->find($user,'id_user');
            
            if( !empty( $posts )){
                
                echo(json_encode($posts));
                
            }else {
                
                $this->notFound();
                
            }
            
        }
     
    
    
    
}

