<?php
namespace app\Controller;

/**
 *
 * @author nicohay
 *        
 */

class UsersController extends AppController
{
    

    public function __construct(){
        
        parent::__construct();

        
        $this->loadModel('User');
        
        
    }
    
    /**
     ********************************************************************
     *
     * Return user info if there is a result 
     * Return 404 Not Found if not
     * 
     * @return MIXED \JsonSerializable // header 404
     * 
     ********************************************************************
     */
    public function user($username) {
        if ( $_SERVER["REQUEST_METHOD"] === "POST" ) {
            
          $user = $this->User->find($username,'nom',true);
          
          if($user){
              
              echo(json_encode($user));
              
          }else{
              
             $this->notFound();
          }
            
            
        }else {
            $this->notAllowed();
            
            
        }
        
         
    
        
    }

    
    
    
    
}