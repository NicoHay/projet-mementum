<?php
namespace app\Controller;


/**
 * @author nicohay
 *        
 */

class PostsController extends AppController
{
    

    
    
    public function __construct(){
        
        parent::__construct();
        
        $this->loadModel('Post');

        
    }

    /**
     ********************************************************************
     *
     * retourne la liste de tous les posts 
     * 
     * @return \JsonSerializable
     * 
     * 
     ********************************************************************
     */
    public function users($param = NULL) {
        
        if ( $_SERVER["REQUEST_METHOD"] === "POST" ) {
            
            $posts = $this->Post->all();
            
            if( !empty( $posts )){
                
                echo(json_encode($posts));
                
            }else {
                
                $this->notFound();
                
            }
            
        }else {
            
            $this->notAllowed();
            
        }      
    }
    
    /**
     ********************************************************************
     *
     * retourne la liste des posts d'un user en particulier (ID)
     *
     * @return \JsonSerializable
     *
     *
     ********************************************************************
     */
    public function user($param){
        
        if ( $_SERVER["REQUEST_METHOD"] === "POST" ) {
            
            $posts = $this->Post->find($param,'id_auteur');
            
            if( !empty( $posts )){
                
                echo(json_encode($posts));
                
            }else {
                
                $this->notFound();
                
            }
                    
        }else {
            
            $this->notAllowed();
            
        }
    }
  
}
