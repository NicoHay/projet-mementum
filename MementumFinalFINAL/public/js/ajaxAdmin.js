$(document).ready(function () {
    // //  ##################################################################################################################################
    // //  ##################################################################################################################################
    // //  ##########################                      ACTIVATE ITEM                            #########################################
    // //  ##################################################################################################################################
    // //  ##################################################################################################################################

    $("#liste_posts").on("click",'.activateItem' ,function (evt) {
        evt.preventDefault();


        var userId = $(this).data("postid");
        var thebtnactive= $(this);
        var thebtnedit= $(this).siblings().eq(0);

        
        $.ajax({
            type: "POST",
            url: "?p=admin.posts.activate&id=" + userId,

            data: {
                userId: userId
            },
            success: function (retour) {

                retour = JSON.parse(retour);


                if (retour.success) {

                    $.fn.flasher(retour.success);
                    thebtnactive.remove();
                    thebtnedit.remove();
             

                } else {

                    $.fn.errorFlasher(retour.alert);

                }

            },
            error: function () {

                //message flash
                $.fn.errorFlasher('Une erreur est survenue veuillez réessayer');

            }
        });
    });

    // //  ##################################################################################################################################
    // //  ##################################################################################################################################
    // //  ##########################                      DELETE ITEM                              #########################################
    // //  ##################################################################################################################################
    // //  ##################################################################################################################################

    $("#liste_posts").on("click",".deleteItem", function (evt) {
        evt.preventDefault();

        var userId = $(this).data("postid");
        var row = $(this).parent().parent().parent().parent().parent();



        $.ajax({
            type: "POST",
            url: "?p=admin.posts.delete",
            data: {
                id: userId
            },
            success: function (retour) {

          
               retour = JSON.parse(retour);

                if (retour.success) {

                    $.fn.flasher(retour.success);
                    row.remove();

                } else {

                    $.fn.errorFlasher(retour.alert);
                    row.remove();

                }

            },
            error: function () {

                //message flash
                $.fn.errorFlasher('Une erreur est survenue veuillez réessayer');

            }
        });
    });

    // //  ##################################################################################################################################
    // //  ##################################################################################################################################
    // //  ##########################                      GET MODAL                                #########################################
    // //  ##################################################################################################################################
    // //  ##################################################################################################################################

    $("#liste_posts").on("click", ".btn-modal",function (evt) {
        evt.preventDefault();
   

        var url = $(this).data("url");
        var userId = $(this).data("postid");


        $.ajax({
            type: "POST",
            url: url,

            data: {
                id: userId
            },
            success: function (retour) {

              retour = JSON.parse(retour);
              
                var modal = ' <div class="form-label-group">';
                modal += ' <input type="text" class="form-control" name="titre_edit" id="titre_edit" value="';
                modal += retour.titre;
                modal += ' " placeholder="Titre" required><label for="titre_edit"></label></div>';
                modal += ' <div class="form-group"><label class="ml-1" for="contenu_edit">contenu de l"article : ';
                modal += '</label><textarea class="form-control" name="contenu_edit" id="contenu_edit" placeholder="Contenu du message" rows="5" required>';
                modal += retour.contenu;
                modal += '</textarea></div>';

                var modalFooter = '<div class="modal-footer">';
                modalFooter += '    <button type="button" class="btn btn-secondary"';
                modalFooter += '    data-dismiss="modal">Fermer</button>';
                modalFooter += '    <button type="" name="edit_form" ';
                modalFooter += '    class="btn btn-primary mr-1 btn-save"';
                modalFooter += '    name="edit_form"';
                modalFooter += '    data-savepostid="'+retour.id+'" >Enregistrer</button>';
                modalFooter += '</div>';

                $('.modal-body').html(modal);
                $('.modal-footer').html(modalFooter);

            },
            error: function () {
                //message flash
                $.fn.errorFlasher('Une erreur est survenue veuillez réessayer');
            }
        });
    });

    // //  ##################################################################################################################################
    // //  ##################################################################################################################################
    // //  ##########################                      EDIT A POST                              #########################################
    // //  ##################################################################################################################################
    // //  ##################################################################################################################################

    $("#liste_posts").on("click", ".btn-save",function (evt) {
        evt.preventDefault();
        
        var titre = $("#titre_edit").val();
        var contenu = $("#contenu_edit").val();
        var saveId = $(this).data("savepostid");
        var titleCard = $(".btn-modal[data-postid='"+saveId+"']").parent().parent().siblings().eq(0);
        var textCard = $(".btn-modal[data-postid='"+saveId+"']").parent().parent().siblings().eq(1);
       
        $.ajax({
            
            type: "POST",                      // methode POST
            url: "?p=admin.posts.edit",        // requête sur le controller admin posts et appel de la fonction edit()
            data: {
                
                titre_edit: titre,
                contenu_edit: contenu,
                saveId : saveId,
            },
            success: function (retour) {
                
                retour = JSON.parse(retour);
                var memo = retour.memo[0]; 
                var message =retour['message'];
      
                if (message.success) {

                    $.fn.flasher(message.success);                  //message flash success
                    $('#modalEdit').modal('hide');                  //fermeture du modal edit
                    titleCard.html(memo.titre);                     //mise a jour du titre du memo
                                                                    //mise a jour du contenu du memo
                    textCard.html('<p class="card-text">'+ memo.contenu  +'<br><a href="index.php?p=admin.posts.show&id='+ memo.id +'"> Voir la suite</a></p>');       
                }else {

                    $.fn.errorFlasher(message.alert);               //message flash error
                }
            },
            error: function () {
              
                $.fn.errorFlasher('Une erreur est survenue veuillez réessayer'); //message flash
            }
        });
    });

    // //  ##################################################################################################################################
    // //  ##################################################################################################################################
    // //  ##########################                     CREATE A POST                              #########################################
    // //  ##################################################################################################################################
    // //  ##################################################################################################################################

    $("#add-post-btn").on("click", function (evt) {
    evt.preventDefault();

        var titre = $("#titre_add").val();
        var contenu = $("#contenu_add").val();

        $.ajax({
            type: "POST",
            url: "?p=admin.posts.add",

            data: {
                titre_add: titre,
                contenu_add: contenu,
            },
            success: function (retour) {

                retour = JSON.parse(retour);
                var memo = retour.memo[0];      
        
            var memoDom  = ' <div class="col-md-4 mt-4">';
                memoDom += '    <div class="card shadow-sm">';
                memoDom += '        <img class="bd-placeholder-img  card-img-top "';
                memoDom += '        src="../public/images/qr/'+  memo.image  +'" alt="Card image cap">';
                memoDom += '        <div class="card-body">';
                memoDom += '            <h5 class="card-title text-center">'+  memo.titre +'</h5>';
                memoDom += '            <p class="card-text">'+ memo.contenu  +'<br><a href="index.php?p=admin.posts.show&id=' + memo.id + '"> Voir la suite</a></p>';
                memoDom += '            <div class="d-flex justify-content-between  align-items-center">';
                memoDom += '                <div class="btn-group " role="group">';
                memoDom += '                    <i class="fas fa-edit btn-modal"  data-postid="';
                memoDom +=                      memo.id +'" data-url="index.php?p=admin.posts.show&id=' + memo.id + '" data-toggle="modal" data-target="#modalEdit" style="font-size: 2em; color: Dodgerblue;"></i>';
                memoDom += '                   <i class="fas fa-check-square Mactive activateItem"';
                memoDom += '                        data-postid="'+  memo.id  +'" style="font-size: 2em; color: #4AA24A;" ></i>';
                memoDom += '                    <i class="fas fa-trash deleteItem" data-postid="'+  memo.id  +'" style="font-size: 2em; color: #df3d3d;"></i>';
                memoDom += '                </div>    ';
                memoDom += '                <div class="modal fade" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="modalEditTitle" aria-hidden="true">';
                memoDom += '                    <div class="modal-dialog modal-dialog-centered" role="document">';
                memoDom += '                        <div class="modal-content">';
                memoDom += '                            <form class="form-signin mt-5" method="post">';
                memoDom += '                                <div class="modal-header">';
                memoDom += '                                    <h5 class="modal-title" id="modalEditTitle">Modifier';
                memoDom += "                                       l\'article </h5>";
                memoDom += '                                    <button type="button" class="close"';
                memoDom += '                                        data-dismiss="modal" aria-label="Close">';
                memoDom += '                                        <span aria-hidden="true">&times;</span>';
                memoDom += '                                    </button>';
                memoDom += '                                </div>';
                memoDom += '                                <div class="modal-body">';
                memoDom += '                                </div>';
                memoDom += '                                <div class="modal-footer">';
                memoDom += '                                    <button type="button" class="btn btn-secondary"';
                memoDom += '                                    data-dismiss="modal">Fermer</button>';
                memoDom += '                                    <button type="" name="edit_form" ';
                memoDom += '                                    class="btn btn-primary mr-1 btn-save"';
                memoDom += '                                    name="edit_form"';
                memoDom += '                                    data-savepostid="'+ memo.id +'">Enregistrer</button>';
                memoDom += '                                </div>';
                memoDom += '                            </form>';
                memoDom += '                        </div>';
                memoDom += '                    </div>';
                memoDom += '                </div>';
                memoDom += '            </div>';
                memoDom += '        </div>';
                memoDom += '    </div>';
                memoDom += '</div>';

                if (retour['message'].success) {

                    $.fn.flasher(retour['message'].success);
                    $("#titre_add").val('');
                    $("#contenu_add").val('');
                    $("#liste_posts > .row").append(memoDom);
  


                } else {

                    $.fn.errorFlasher(retour.alert);

                }
            },
            error: function () {

                //message flash
                $.fn.errorFlasher('Une erreur est survenue veuillez réessayer');

            }
        });
    });
});