<?php

use core\Database\MysqlDatabase;
use core\Config;

class App
{
    
    public $title = 'Memo AR';
    private $db_instance;
    private static $_instance;
    
    
    public static function getInstance(){

        if (is_null(self::$_instance)) {
            self::$_instance = new App();
        }
        return self::$_instance;
    }
    

    public function getTable(string $name){

        $class_name = "\\app\\Table\\" . \ucfirst($name) . 'Table';
        
        return new $class_name($this->getDb());
    }
    /**
     * *********************************************************
     * 
     * Get an instance of the database
     * connect it with config file
     * 
     * @return core\Database\MysqlDatabase
     * 
     * *********************************************************
     */
    public function getDb(){
        
        $config = Config::getInstance(ROOT . '/config/config.php');
        if ($this->db_instance === null) {
            
            $this->db_instance = new MysqlDatabase(
                $login = $config->get('user'),
                $mdp = $config->get('pass'),
                $dns = $config->get('dsn')
                );
        }
        
        return $this->db_instance;
    }
    /**
     * *********************************************************
     * 
     * launch the application 
     * autoloader  + start a session
     *
     * @return VOID
     * 
     * *********************************************************
     */
    public static function load(){
        
        session_start();
        require ROOT . '/app/Autoloader.php';
        app\Autoloader::register();
        require ROOT . '/core/Autoloader.php';
        core\Autoloader::register();
    }
    /**
     * *********************************************************
     * 
     * token generator
     * the lenght is going to be multiply by 2 
     *
     * @param INT $lenght
     *  
     * @return STRING
     * 
     * *********************************************************
     */
    public static function tokenGen(int $length){
        
        $token = openssl_random_pseudo_bytes($length);
        $token = bin2hex($token);
        
        return  $token;
    }

    public static function cleanInput($string){

        return htmlspecialchars($string);
    }
}
