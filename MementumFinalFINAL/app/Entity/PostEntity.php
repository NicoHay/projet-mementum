<?php
namespace app\Entity;

/**
 *
 * @author nicohay
 *        
 */

use core\Entity\Entity;

class PostEntity extends Entity
{
    
    
    
    
    /**
     * ************************************************************
     * 
     * return the post URL with his ID
     *
     * @return STRING
     * 
     * ************************************************************
     */
    public function getUrl()
    {
        
        return 'index.php?p=admin.posts.show&id=' . $this->id;
    }
    /**
     * ************************************************************
     *
     * get a post extract
     *
     * @return STRING
     *
     * ************************************************************
     */
    public function getExtrait()
    {
        $html =  \substr($this->contenu, 0, 100) . '...';
        $html .= '<br><a href="' . $this->getUrl() . '"> Voir la suite</a>';
        return $html;
    }
    
    /**
     * ************************************************************
     *
     * get the post title
     *
     * @return STRING
     *
     * ************************************************************
     */
    public function getTitre(){
        
        return $this->titre;
    }
    
    /**
     * ************************************************************
     *
     * get the post content
     *
     * @return STRING
     *
     * ************************************************************
     */
    public function getContenu(){
        
        return $this->contenu;
    }

}
