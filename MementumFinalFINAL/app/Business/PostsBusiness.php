<?php
namespace app\Business;

include ( ROOT.'/app/library/phpqrcode/qrlib.php');

use app\library\phpqrcode\qrlib;

use core\Database\VuforiaDatabase;


/**
 *
 * @author nicohay
 *        
 */
class PostsBusiness
{

/**
 *****************************************************************
 * 
 * permet de generer un Qr code avec comme contenu l'id de l'article
 * et enregistre le fichier ds le dossier images/qr
 * 
 * @param STRING $name nom du fichier
 * @param STRING $contenu contenu du Qr code
 * 
 * @return VOID
 * 
 ******************************************************************
 */
    public function QrGen($name,$contenu){

       
       $chemin = '../public/images/qr/'.$name;
        \QRcode::png(  $contenu ,$chemin , 'H' , 6, 6 ); 
                    
        $this->convertTo8Bit($chemin , $chemin);
            
    }
    
   
/**
 *****************************************************************
 * 
 * transforme le qr code en noir et blanc
 * 
 * @param STRING $path chemin du fichier
 * 
 * @return VOID
 * 
 ******************************************************************
 */
    public function colorToGrey( $chemin ){

                
        
        $image =  imagecreatefrompng( $chemin ) ;
        
        imagefilter($image, IMG_FILTER_GRAYSCALE);
        imagepng($image, $chemin);
        
        $this->convertImgMode($chemin);   

        
   }
/**
 *****************************************************************
 * 
 * converti en 8bit
 * 
 * @param STRING $sourcePath chemin du fichier
 * @param STRING $destPath chemin de destination du fichier
 * 
 * @return VOID
 * 
 ******************************************************************
 */
    public function convertTo8Bit ($sourcePath, $destPath) {
        
        $srcimage = imagecreatefrompng($sourcePath);
        list($width, $height) = getimagesize($sourcePath);
        
        $img = imagecreatetruecolor($width, $height);
        $bga = imagecolorallocatealpha($img, 0, 0, 0, 127);
        imagecolortransparent($img, $bga);
        imagefill($img, 0, 0, $bga);
        imagecopy($img, $srcimage, 0, 0, 0, 0, $width, $height);
        imagetruecolortopalette($img, false, 255);
        imagesavealpha($img, true);
        
        imagepng($img, $destPath);
        imagedestroy($img);
        
        $this->colorToGrey($destPath);
        
    }
    
/**
 *****************************************************************
 * 
 * converti en mode grayscale l'image
 * 
 * @param STRING $sourcePath chemin du fichier
 * 
 * @return VOID
 * 
 ******************************************************************
 */
    public function convertImgMode($path){
        
        
        
        $img = new \Imagick();
        $img->readImage($path);
        $img->setImageType(\Imagick::IMGTYPE_GRAYSCALE);
        $img->writeImage($path);
    }
    
/**
 *****************************************************************
 * 
 * permet denvoyer un Qr code avec comme contenu l'id de l'article
 * 
 * @param STRING $name nom du fichier
 * @param STRING $contenu contenu du Qr code
 * 
 * @return VOID
 * 
 ******************************************************************
 */
    public function envoiVuforia($img,$data){
    
        $vufo           = new VuforiaDatabase();
        return  $vufo->addTarget( $img , $data );
            
    }
/**
 *****************************************************************
 * 
 * supprime un enregistrement dans vuforia
 * 
 * @param INT $idTarget numero de la target
 * 
 * @return deleteTarget() function
 * 
 ******************************************************************
 */
    public function deleteVuforia($idTarget){

        
        $vufo           = new VuforiaDatabase();
        return $vufo->deleteTarget( $idTarget );
               
    }
/**
 *****************************************************************
*
* retourne l'id de la target enregistrer dans vuforia
* 
* @param String $response 
* 
* @return STRING  
*****************************************************************
*/
    public function formatVuforiaResponse($response){
        
        $responseExplode    = explode('"' , $response );
        return $responseExplode[11];
 

    }
/**
 *****************************************************************
 * 
 * supprime le fichier du dossier 
 * 
 * @param STRING $path chemin du fichier
 * 
 * @return VOID
 * 
 ******************************************************************
 */
    public function deleteFile($path){


        
        $chemin = '../public/images/qr/'.$path;
        \unlink($chemin);
        
    }

}



