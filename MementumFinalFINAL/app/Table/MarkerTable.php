<?php
namespace app\Table;

use core\Table\Table;
/**
 *
 * @author nicohay
 *        
 */
class MarkerTable extends Table{
    
    
    protected $table = "markers";
  
    
/**
********************************************************************
*
* delete a post with his ID
*
* @param INT $id
* 
* @return VOID
* 
* ******************************************************************
*/
    public function deleteByIdInfo($id) {
        
        return $this->query("DELETE FROM {$this->table} WHERE id_info = ?", [$id], true);
    }
}

