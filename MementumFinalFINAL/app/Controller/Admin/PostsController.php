<?php
namespace app\Controller\Admin;

use app\Business\PostsBusiness;
use app;

/**
 *
 * @author nicohay
 *        
 */
class PostsController extends AppController
{

    public function __construct()
    {

        parent::__construct();

        $this->loadModel('Post');
        $this->loadModel('Marker');

    }

    /**
     ********************************************************************
     *
     * genere la vue de l'index
     * et recupere les articles postés
     * 
     * @return VOID
     * 
     ********************************************************************
     */
    public function index($message  = null)
    {

        $posts  = $this->Post->find($_SESSION['auth'], 'id_auteur');
        $this->render('admin.posts.index', compact('posts', 'message'));
    }

    /**
     ********************************************************************
     *
     * permet de gerer l'ajout d' un article
     *
     * @return void
     * 
     ********************************************************************
     */
    public function add()
    {

        $businessPost                 = new PostsBusiness();
        $token                        = App::tokenGen(4);

        if ( !empty($_POST['titre_add']) && !empty($_POST['contenu_add']) ) {

            $name             = App::cleanInput($_POST['titre_add']) . '_' . $_SESSION['auth'] . '_' . $token . '.png';
            $pathImg          = '../public/images/qr/';

            $result = $this->Post->create([
                        'titre'         => App::cleanInput($_POST['titre_add']),
                        'contenu'       => App::cleanInput($_POST['contenu_add']),
                        'id_auteur'     => $_SESSION['auth'],
                        'image'         => $name,

            ]);

            if ($result) {

                $contenuQr    = $this->Post->lastInsertId();
                $businessPost->QrGen($name, $contenuQr);
                
                $memo = $this->Post->find($contenuQr);
                $message      = $this->setMessage('success', "L'article a bien été creé");

                $data = ([  "memo"=> $memo,
                            "message"=> $message]);
                echo \json_encode($data);        
                
            }else{

                $message      = $this->setMessage('alert', "Une erreur est survenue veuillez essayer a nouveau ");
                echo \json_encode($message);
              
            }
        }
    }


    /**
     ********************************************************************
     *
     * permet d'editer / modifier un article
     *
     * @return VOID
     * 
     ********************************************************************
     */
    public function edit(){

 
  
     
        if (!empty($_POST)) {
     
      
            $result = $this->Post->update(App::cleanInput($_POST['saveId']), [
                        'titre'     => App::cleanInput($_POST['titre_edit']),
                        'contenu'   => App::cleanInput($_POST['contenu_edit']),
            ]);

            if ($result) {

                $memo = $this->Post->find(App::cleanInput($_POST['saveId']));
                $message    = $this->setMessage('success', " L'article à été modifié ");
                $data = ([  "memo"=> $memo,
                            "message"=> $message]);

                echo \json_encode($data);
               
            }
        }
        

    }

    /**
     ********************************************************************
     *
     * permet d'effacer un article
     *
     * @return VOID
     * 
     ********************************************************************
     */
    public function delete(){

        if (!empty($_POST)) {

            $businessPost   = new PostsBusiness();
       

            $cleanid = App::cleanInput( $_POST['id'] );

            $post           = $this->Post->find( $cleanid, 'id', true);

 
            $businessPost->deleteFile($post->image);
            $this->Post->delete( $cleanid);        
                                
            if ($post->online == 1) {

                $marker     = $this->Marker->find( $cleanid, 'id_info', true);
                $this->Marker->deleteByIdInfo( $cleanid);
                $businessPost->deleteVuforia($marker->marker);
       


                $message              = $this->setMessage('success', " L'article a été supprime ");


                echo \json_encode($message);
                
            }else{

                $message              = $this->setMessage('alert', " L'article n'a pas pu être supprimé ");
                echo (json_encode($message));
            }


            
        
            }
        }
    /**
     ********************************************************************
     *
     * permet d'afficher un article en particulier
     * 
     *
     * @return VOID
     *
     ********************************************************************
     */
    public function show()
    {

        $post   = $this->Post->find(App::cleanInput($_GET['id']), 'id', true);

        if ($post === false) {

            $this->notFound();
        }

        echo \json_encode($post);
    }

    /**
     ********************************************************************
     *
     *  permet d'activer un article (online) 
     *      + Envoi du QR en bdd Vuforia
     *      + enregistrement de la target id du Qr en bdd
     *
     * @return VOID
     *
     ********************************************************************
     */
    public function activate()
    {

        $businessPost = new PostsBusiness();
        $cleanid = $_GET['id'];

        $result = $this->Post->update($cleanid, [
            'online' => "1",
        ]);

        if ($result) {

            $article  = $this->Post->find($cleanid, 'id', true);
            $active   = $businessPost->envoiVuforia($article->image, $article->contenu);

            if ($active === false) {

                $message  = $this->setMessage('alert', " une erreur est arrivée pendant l'activation du memo ");
                echo \json_encode($message);

            } else {

                $idtarget             = $businessPost->formatVuforiaResponse($active);
                $this->Marker->create([
                    'id_info' => $article->id,
                    'id_user' => $article->id_auteur,
                    'marker'  => $idtarget
                ]);

                $message   = $this->setMessage('success', " L'article a ete active ");

                echo \json_encode($message);
            }

        }
    }
}
