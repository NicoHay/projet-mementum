<?php
namespace app\Controller\Admin;

use app;
use core\Auth\DBAuth;
/**
 *
 * @author nicohay
 *        
 */
class AppController extends \app\Controller\AppController
{
    protected $template = 'admin';
  
    public function __construct(){
        
        
        parent::__construct();
         
        $app = App::getInstance();
        $auth = new DBAuth($app->getDb());
        
        if(!$auth->logged()){
            
           header('Location: ?p=users.login');
        }
        
    }
    
    
    
    
   
}

