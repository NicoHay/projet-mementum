<?php
namespace app\Controller\Publicc;

use app ;

/**
 *
 * @author nicolas
 *        
 */
class PubliccController extends AppController
{


    
    public function __construct(){
        
        parent::__construct();
        
        $this->loadModel('Messagerie');
    }

    /**
     * 
     * **************************************************************
     * 
     * return index page
     *
     * @return void
     * 
     * *************************************************************
     */

    public function index() {

        
     
        $this->render('publicc.index' , compact( 'message' ));
       
        
    
    
    }

    /**
     * *************************************************************
     * handle the contact form
     * 
     *
     * @return VOID
     * 
     * **********************************************************
     */
    public function contactForm(){


    
        if(!empty($_POST['contactMail']) && ($_POST['contactMail'] == 'ok')) {

            $result = $this->Messagerie->create([
                
                'nom_contact'             => $_POST['contact_nom'],
                'prenom_contact'           => $_POST['contact_prenom'],
                'email_contact'           => $_POST['contact_email'],
                'message_contact'           => $_POST['contact_message'],
            
                ]);

            if($result) {
                
                $message =  $this->setMessage('success', 'Merci pour votre email');

                echo \json_encode($message);

            }else{

                $message =  $this->setMessage('alert', 'Une erreur est survenue merci de réessayer');

                echo \json_encode($message);
            }

        }
        

    }


}
 
