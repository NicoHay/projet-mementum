<?php
namespace app\Controller;

use core\Auth\DBAuth;
use App;

/**
 *
 * @author nicohay
 *        
 */
class UsersController extends AppController
{
    

    public function __construct(){
        
        parent::__construct();

        
        $this->loadModel('User');
        
        
    }
    
    /**
     ********************************************************************
     *
     * gestion de la partie login 
     * traitement du formulaire de login
     * si la reponse est ok envoie sur la page index de la partie admin
     * sinon retourne une erreur
     *
     * @return VOID
     * 
     ********************************************************************
     */
    public function login() {
        
            $app = App::getInstance();           
             $auth = new DBAuth($app->getDb());

             if ($auth->logged()) {
             
                 $this->render('admin.posts.index'  );
                 
            }else{
            
                if(!empty($_POST['login_form'])){
                
                
                    if($auth->login( App::cleanInput($_POST['username_login']) , App::cleanInput($_POST['password_login'] ))){
                        
                        header('Location: ?p=admin.posts.index' );

                        
                    }
                $message =  $this->setMessage('alert', 'mot de passe ou login incorrect');
                
            }
            // $this->render( 'users.register' );      
            $this->render('users.login', compact('message') );
            }
    }

    /**
     ********************************************************************
     *Deconnexion de l'utilisateur
     *      destruction de la session et envoi sur acceuil
     *
     * @return VOID
     *
     ********************************************************************
     */
    public function logout(){
        
        $message =  $this->setMessage('alert', 'Vous êtes maintenant déconnecter');
        
        $this->render('publicc.index' ,  compact('message'));
        
        session_destroy();
    }
    /**
     ********************************************************************
     *
     * gestion de la partie inscription
     * traitement du formulaire d'inscription
     * 
     * -> appel a la fonction controlPass
     * -> appel a la fonction hashPass
     * 
     * enregistre lutilisateur en BDD
     *
     * @return VOID
     * 
     ********************************************************************
     */
    public function register() {
        
        $message= array();
        $result = false;
        $app = App::getInstance();
        $auth = new DBAuth($app->getDb());
        
        if(!empty($_POST['registerForm'])) {

            
            $validPass = $this->controlPass(App::cleanInput($_POST['pass_register']), App::cleanInput($_POST['confirm_pass_register']));
            
            if ($validPass === TRUE) {
                
                $passHash = $auth->hashPass(App::cleanInput($_POST['confirm_pass_register']));
                      
                $result = $this->User->create([
                    'nom'           => App::cleanInput($_POST['nom_register']),
                    'prenom'        => App::cleanInput($_POST['prenom_register']),
                    'email'         => App::cleanInput($_POST['email_register']),
                    'pass'          => $passHash
                ]);
                
            }
                
                $message =  $this->setMessage('alert', 'Les mots de passe sont différents ou ne repondent pas aux critères');
                
               
                               
                if ($result) {
                    
                    $message =  $this->setMessage('success', 'Vous êtes inscrit avec succes');
                    
                  
                  //  $this->render( 'users.login' , compact('message') );   
                   
                }     

                
        }     
        
        $this->render( 'users.register' , compact('message') );      
    }
    
    
    /**
     ********************************************************************
     *
     * gestion d'oubli du mot de passe 
     * traitement du formulaire 
     * 
     * -> appel a la fonction mailPassForget 
     *
     * @return VOID
     * 
     ********************************************************************
     */
    public function forget() {
        
        if (!empty($_POST['forget_form'])) {
            
            
            $users   = $this->User->find(App::cleanInput($_POST['email_forget']),'email');

            if($users){
                
                $token = App::tokenGen(40);     
                $date = date("Y-m-d H:i:s");
               
                $this->User->update($users->id,[
                    "reset_token"       => $token ,
                    "reset_at"          => $date ,      
                ]);
                $this->mailPassForget( $users->email , $users->id , $token );
                
                $message =  $this->setMessage('success', "Un mail a eté envoyer avec les details à l'adresse renseignée");
                
                $this->render( 'users.login'  , compact('message'));
            }
            $message =  $this->setMessage('success', "Un mail a eté envoyer avec les details à l'adresse renseignée");
        }
        
        
        $this->render( 'users.forget' , compact('message'));
    }
    
    /**
     ********************************************************************
     *
     * gestion la fonction reset mot de passe 
     * traitement du formulaire
     *
     * TODO! LOL refactoriser tout ca cest le bordel MAIS ca marche :)
     *
     * @return VOID
     *
     *********************************
     */
    public function reset() {
        
        $app = App::getInstance();
        $auth = new DBAuth($app->getDb());
        
        // control de la validiter du token 
        $validToken = $this->User->findToken(App::cleanInput($_GET['id']),App::cleanInput($_GET['token']),"reset_token");
        
        if($validToken){
        
            if (!empty($_POST['reset_form'])) {
                
                $validPass = $this->controlPass(App::cleanInput($_POST['pass_reset']), App::cleanInput($_POST['confirm_pass_reset']));
                
                if ( $validPass ) {
                    
                    $passHash = $auth->hashPass(App::cleanInput($_POST['confirm_pass_reset']));
                    
                    $this->User->update( App::cleanInput($_GET['id']) , [
                        'pass'          => $passHash ,
                        'reset_token'   => NULL ,
                        'reset_at'      => NULL ,
                    ]); 
                    
                    
                    // les deux mots de passe sont identique donc renvoie sur la page login 
                    $this->login();
                
                }else{
                    
                    $this->render('users.reset');
                    
                }
                
            //    mot de passe differents
                $message =  $this->setMessage('alert', " Les deux mot de passe saisis sont differents");
              }
              
        
            // id et token correspondent donc affichage de la page reset
            $this->render('users.reset');
            
        }else{
            
            $message =  $this->setMessage('alert', "le lien semble corrompu ou le délai a été depassé");
            $this->render('users.forget' , compact('message'));
            
        }
         
    }
    
    /**
     ********************************************************************
     *
     * Controlle que les deux mots de passe entrés soit identiques
     *  ET si le mot de passe comporte 1 chiffre / 1 majuscule /  plus de 8 characteres
     * 
     *  @param STRING $pass1
     *  @param STRING $pass2
     *
     * @return BOOLEAN
     * 
     ********************************************************************
     */
    protected function controlPass( $pass1 , $pass2 ) {
        
        
        $verify = preg_match('#^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{8,}$#',$pass1); 
        
        if (($pass1 === $pass2) &&($verify === 1) ) {
            
            return TRUE;
        }
        
        return FALSE;
        
    }
            
    /**
     ********************************************************************
     *
     * Envoi du mail avec le token et l'id de la personne qui le demande
     *
     *  @param STRING $email de l'user qui fait la demande
     *  @param STRING $id de l'user
     *  @param STRING $token aleatoire
     *
     * @return VOID
     *
     ********************************************************************
     */
    protected function mailPassForget($email,$id,$token){
           
 // TODO: modifier le chemin du mail 

        mail($email,"Réinitialisation de votre mot de passe Memo AR","Bonjour, \n
        Suite à votre demande , Vous pouvez redéfinir votre mot de passe, en cliquant sur le lien suivant et en suivant les instructions : \n
        http://localhost/github/travauxCoda/MementumFinal/public/index.php?p=users.reset&id=$id&token=$token
        Nous vous remercions pour votre confiance. \n
        L'équipe Mementum.");
        
    }
    
}