<!doctype html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


	<title>Mementum | Dashboard</title>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr"
	 crossorigin="anonymous">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	 crossorigin="anonymous">
	
	<link rel="stylesheet" href="../public/css/style.css">
  <link rel="stylesheet" href="../public/css/admin.css">
  <link rel="stylesheet" href="../public/css/base.css">
    
	<script src="../public/js/scroll-out.min.js"></script>

	<script src="../public/js/ajaxAdmin.js"></script>
	<script src="../public/js/jquery-dotimeout-min.js"></script>
  <script src="../public/js/libnh.js"></script>
  <!-- favicon -->
  <link rel="icon" href="../public/images/favicon.ico" />
</head>



<body>
	<nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
		<a class="navbar-brand col-sm-3 col-md-2 mr-0 text-center nom_appli " href="index.php?p=publicc.index"><i class="fas fa-home "></i> MEMO AR</a>
		<ul class="navbar-nav px-3">
			<li class="nav-item text-nowrap mr-3">
				<a class="nav-link" href="?p=users.logout"><i class="fas fa-user-lock"></i> Déconnexion</a>
			</li>
		</ul>
	</nav>

	<div class="container-fluid">
		<div class="row">
	

				<!-- ####################################       contenu de la page       ########################################################-->
				<?= $content ?>

			 
		</div>
	</div>



  
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
    crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
    crossorigin="anonymous"></script>
</body>

</html>