<div class="container-fluid">
    <div class="row">

        <nav class="col-md-2 d-none d-md-block bg-sidebar sidebar">
            <div class="sidebar-sticky">

                <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-3 text-muted">
                    <span>Mon espace</span>
                    <span class="message_flash"></span>
                </h6>

                <ul class="nav nav-tabs flex-column">

                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#liste_posts" role="tab"
                            aria-controls="liste_posts" aria-selected="false">
                            <span data-feather="file"></span> <span class="sr-only">(current)</span>
                            Mes articles
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " data-toggle="tab" href="#ajouter_article" role="tab"
                            aria-controls="ajouter_article" aria-selected="false">
                            <span data-feather="file"></span>
                            Ajouter un article
                        </a>
                    </li>

                </ul>
            </div>
        </nav>

        <main role="main" class="ml-sm-auto col-12 col-md-10  px-4 ">

        	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap ">

        		<div class="tab-content">

        			<div class="mt-2 sticky">
        				<h1 class="h2">Tableau de bord</h1>
        				<hr>
        			</div>
        			<div class="tab-pane show active " id="liste_posts" role="tabpanel"
        			aria-labelledby="liste_posts-tab">
        				<div class="row">
        					<?php foreach ($posts as $post) : ?>
        					<div class="col-md-4 mt-4">
        						<div class="card shadow-sm">
        							<img class="bd-placeholder-img  card-img-top "
        							src="../public/images/qr/<?= $post->image; ?>" alt="Card image cap">
        							<div class="card-body">
        								<h5 class="card-title text-center"> <?= $post->titre; ?></h5>
        								<p class="card-text"><?= $post->extrait; ?></p>
        								<div class="d-flex justify-content-between  align-items-center">
        									<div class="btn-group " role="group">
        										<?php if ($post->online == 0) { ?>

													<i class="fas fa-edit btn-modal" data-postid="<?=$post->id?>" data-url="<?=$post->url?>" 
												data-toggle="modal" data-target="#modalEdit" style="font-size: 2em; color: Dodgerblue;"></i>
												<i class="fas fa-check-square Mactive activateItem"data-postid="<?= $post->id; ?>" style="font-size: 2em; color: #4AA24A;" ></i>        									
        										<?php } ?>
												<i class="fas fa-trash deleteItem" data-postid="<?=$post->id;?>" style="font-size: 2em; color: #df3d3d;"></i>
											</div>
											<?php if ($post->online == 0) { ?>
        									<!-- test modal start -->
        									<div class="modal fade" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="modalEditTitle" aria-hidden="true">
        										<div class="modal-dialog modal-dialog-centered" role="document">
        											<div class="modal-content">
        												<!-- debut du form -->
        												<form class='form-signin mt-5' method="post">
        													<div class="modal-header">
        														<h5 class="modal-title" id="modalEditTitle">Modifier
        															l'article </h5>
        														<button type="button" class="close"
        															data-dismiss="modal" aria-label="Close">
        															<span aria-hidden="true">&times;</span>
        														</button>
        													</div>
        													<!-- modal body start -->
        													<div class="modal-body">

        													</div>
        													<!-- modal body end -->
        													<div class="modal-footer">
        														<button type="button" class="btn btn-secondary"
        														data-dismiss="modal">Fermer</button>
        														<button type="" name="edit_form" 
        														class="btn btn-primary mr-1 btn-save"
        														name="edit_form"
        														data-savepostid="<?=$post->id?>" >Enregistrer</button>
        													</div>
        													<!-- fin du form -->
        												</form>
        											</div>
        										</div>
        									</div>
        									<?php } ?>
        								</div>
        							</div>
        						</div>
        					</div>
        				<?php endforeach; ?>
        				</div>
        			</div>



        			<div class="tab-pane " id="ajouter_article" role="tabpanel" aria-labelledby="ajouter_article-tab">
        				<div class="col-md-12  col-sm-12 mt-5  " id="add-form">
        					<h5 class="mt-2 mb-2">Laissez place à votre imagination <i class="fas fa-pen float-right fa-lg"></i></h5>
        					<hr>
        					<form class='form-signin mt-2' id="add_post" method="post">
			        			<div class="form-label-group">
			        				<input type="text" class="form-control" name="titre_add" id="titre_add"
			        				placeholder="Titre" required>
			        				<label for="titre_add">Intitulé</label>
			        			</div>

			        			<div class="form-group">
			        				<label class="ml-1" for="contenu_add">Contenu de l'article:</label>
			        				<textarea class="form-control" name="contenu_add" id="contenu_add" rows="5"
			        				required></textarea>
			        			</div>

			        			<div class="text-right">
			        				<input type="submit" class="btn btn-dark " id="add-post-btn" value="Créer"
			        				name="add_form">
        					</form>
        				</div>
        			</div>
    			</div>
			</div>
		</main>
    </div>
</div>
