<div class="container-fluid mt-5 mb-5">
    <h1 class='text-center'>Oubli de mot de passe.</h1>
    <div class="row justify-content-center " id="add_post">
        <div class="col-md-5 mt-5  " id="add-form">

            <h5 class="mt-5 mb-2">Entrez votre mot de passe et suivez les instructions<i class="fas fa-envelope float-right fa-lg"></i></h5>
            <hr>
            <span class="temp">
            <?php if(isset($message['alert'])) {
                echo $message['alert'];
            }else if(isset($message['success'])){
                echo $message['success'];
            } ?>
            </span>
            <form class='form-signin mt-5' method="post">
                <div class="form-label-group">
                    <input type="email" class="form-control" name="email_forget" id="email_forget" placeholder="Email"
                        required>
                    <label for="username_login">Email</label>
                </div>

                <button type="submit" class="btn btn-primary" name="forget_form">Valider</button>
            </form>
        </div>
    </div>
</div>






