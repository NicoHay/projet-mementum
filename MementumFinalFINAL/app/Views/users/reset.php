<div class="container-fluid mt-5 mb-5">
    <h1 class='text-center'>Redefinissez votre mot de passe.</h1>
    <div class="row justify-content-center " id="add_post">
        <div class="col-md-5 mt-5  " id="add-form">

            <h5 class="mt-5 mb-2">Entrez votre mot de passe et suivez les instructions<i class="fas fa-envelope float-right fa-lg"></i></h5>
            <hr>
            <span class="temp">
            <?php if(isset($message['alert'])) {
                echo $message['alert'];
            }else if(isset($message['success'])){
                echo $message['success'];
            } ?>
            </span>
            <form class='form-signin mt-5' method="post">
                <div class="form-label-group">
                    <input type="password" class="form-control" name="pass_reset" id="pass_reset" placeholder="Nouveau mot de passe"
                        required>
                    <label for="pass_reset">Nouveau mot de passe</label>
                </div>

                <div class="form-label-group">
                    <input type="password" class="form-control" name="confirm_pass_reset" id="confirm_pass_reset" placeholder="Retapez Votre mot de passe"
                        required>
                    <label for="confirm_pass_reset">Retapez Votre mot de passe</label>
                </div>

                <input type="submit" class="btn btn-primary" name="reset_form">
            </form>
        </div>
    </div>
</div>





