<div class="container-fluid mt-5 mb-5">
    <h1 class='text-center'>Inscription</h1>
    <div class="row justify-content-center " id="add_post">
        <div class="col-md-5 mt-5  " id="add-form">

            <h5 class="mt-5 mb-2">Inscrivez-vous en 2 clics<i class="fas fa-user float-right fa-lg"></i></h5>
            <hr>
            <span class="temp">
            <?php if(isset($message['alert'])) {
                echo $message['alert'];
            }else if(isset($message['success'])){
                echo $message['success'];
            } ?>
            </span>
            <form class='form-signin mt-5' method="post">
                <div class="form-label-group">
                    <input type="text" class="form-control" name="nom_register" id="nom_register" placeholder="Nom / Entreprise"
                        required>
                    <label for="nom_register">Nom / Entreprise</label>
                </div>
                <div class="form-label-group">
                    <input type="text" class="form-control" name="prenom_register" id="prenom_register" placeholder="Prénom"
                        required>
                    <label for="prenom_register">Prénom</label>
                </div>
                <div class="form-label-group">
                    <input type="email" class="form-control" name="email_register" id="email_register" placeholder="Email"
                        required>
                    <label for="email_register">Email</label>
                </div>

                <div class="form-label-group">
                    <input type="password" class="form-control" name="pass_register" id="pass_register" placeholder="Mot de passe"
                        required>
                    <label for="pass_register">Mot de passe</label>
                </div>

                <div class="form-label-group">
                    <input type="password" class="form-control" name="confirm_pass_register" id="confirm_pass_register" placeholder="Confirmation du mot de passe"
                        required>
                    <label for="confirm_pass_register">Confirmation du mot de passe</label>
                </div>

                <input type="submit" class="btn btn-primary" name="registerForm">
            </form>
        </div>
    </div>
</div>