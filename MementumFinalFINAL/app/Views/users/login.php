<div class="container-fluid mt-5 mb-5">
    <h1 class='text-center'>Connection.</h1>
    <div class="row justify-content-center " id="login_form">
        <div class="col-md-5 mt-5  " id="add-form">

            <h5 class="mt-5 mb-2">Connectez-vous a votre espace personnel <i class="fas fa-user-lock float-right fa-lg"></i></h5>
            <hr>
            <span class="temp">
            <?php if(isset($message['alert'])) {
                echo $message['alert'];
            }else if(isset($message['success'])){
                echo $message['success'];
            } ?>
            </span>
            <form class='form-signin mt-5' method="post">
                <div class="form-label-group">
                    <input type="text" class="form-control" name="username_login" id="username_login" placeholder="Nom utilisé lors de l'inscription">
                    <label for="username_login">Nom</label>
                </div>

                <div class="form-label-group">
                    <input type="password" class="form-control" name="password_login" id="password_login" placeholder="Mot de passe"
                        required>
                    <label for="password_login">Mot de passe</label>
                </div>
                <div class="form-label-group">
                    <a href="index.php?p=users.forget"> Mot de passe oublié ? </a>
                </div>
                <input type="submit" class="btn btn-primary" name="login_form">
            </form>
        </div>
    </div>
</div>



