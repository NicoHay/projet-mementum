-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Client :  localhost:3306
-- Généré le :  Jeu 03 Janvier 2019 à 11:54
-- Version du serveur :  5.7.24-0ubuntu0.18.04.1
-- Version de PHP :  7.2.10-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `nicoProjet`
--

-- --------------------------------------------------------

--
-- Structure de la table `auth`
--

CREATE TABLE `auth` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_user` int(11) UNSIGNED NOT NULL,
  `session_token` varchar(50) DEFAULT NULL,
  `session_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `informations`
--

CREATE TABLE `informations` (
  `id` int(11) UNSIGNED NOT NULL,
  `titre` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `contenu` text COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_ajout` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_auteur` int(11) UNSIGNED DEFAULT NULL,
  `online` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `informations`
--

INSERT INTO `informations` (`id`, `titre`, `contenu`, `image`, `date_ajout`, `id_auteur`, `online`) VALUES
(21, 'test2', 'ceci est le contenu du test2', 'test2_6_2a471feb.png', '2018-12-19 13:06:29', 6, 1),
(22, 'fonctionne', 'ca marche ? \r\n', 'fonctionne_6_a8b7f764.png', '2018-12-19 13:54:46', 6, 1),
(37, 'bonne annee', 'meilleurs voeuxmeilleurs voeuxmeilleurs voeuxmeilleurs voeuxmeilleurs voeuxmeilleurs voeuxmeilleurs voeuxmeilleurs voeux', 'bonne annee_4_4ac74a02.png', '2019-01-02 14:15:57', 4, 1),
(38, 'article numero undefined', '46546546546546546546544654654654654654654654465465465465465465465446546546546546546546544654654654654654654654', 'article numero undefined_4_19ec72bf.png', '2019-01-03 09:27:31', 4, 1),
(39, 'article de noel', '6969696969696969696', 'article de noel_4_7ffb4e31.png', '2019-01-03 11:47:06', 4, 1);

-- --------------------------------------------------------

--
-- Structure de la table `markers`
--

CREATE TABLE `markers` (
  `id` int(11) NOT NULL,
  `id_info` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `marker` varchar(60) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `markers`
--

INSERT INTO `markers` (`id`, `id_info`, `id_user`, `marker`) VALUES
(2, 196, 6, '1ec3a5a095b749df9a7c42bf9286f131'),
(3, 197, 6, 'a5836d35f32541a1924edee89cb6a7ca'),
(12, 21, 6, '00f71e2de887499b9c37aaef3c53d7c7'),
(13, 22, 6, 'cc0833dbb8c2429c8cc46323117dacd2'),
(15, 37, 4, '2510d15320404aed9fc01b624ade0675'),
(16, 38, 4, 'f8edb349e6874b449b7c4241ad2b7760'),
(17, 39, 4, '97a35a8abc7d496095fa4e4e021a1e9b');

-- --------------------------------------------------------

--
-- Structure de la table `messagerie`
--

CREATE TABLE `messagerie` (
  `id` int(11) UNSIGNED NOT NULL,
  `nom_contact` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `prenom_contact` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email_contact` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `message_contact` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `messagerie`
--

INSERT INTO `messagerie` (`id`, `nom_contact`, `prenom_contact`, `email_contact`, `message_contact`) VALUES
(1, 'nico', 'no', 'blabla@hot.com', 'elnzfkjrneelnzfkjrneelnzfkjrneelnzfkjrneelnzfkjrneelnzfkjrneelnzfkjrneelnzfkjrneelnzfkjrneelnzfkjrneelnzfkjrneelnzfkjrneelnzfkjrneelnzfkjrneelnzfkjrneelnzfkjrneelnzfkjrneelnzfkjrneelnzfkjrneelnzfkjrneelnzfkjrneelnzfkjrneelnzfkjrneelnzfkjrne'),
(2, 'nico', 'no', 'blabla@hot.com', 'ggfdg'),
(3, 'nico', 'no', 'blabla@hot.com', 'ggfdg'),
(4, 'vnbvc', 'nbvnbvn', 'fbbfd@hot.com', 'vcxvxcccccccccccccccccvcxvxcccccccccccccccccvcxvxcccccccccccccccccvcxvxcccccccccccccccccvcxvxcccccccccccccccccvcxvxcccccccccccccccccvcxvxcccccccccccccccccvcxvxcccccccccccccccccvcxvxcccccccccccccccccvcxvxcccccccccccccccccvcxvxcccccccccccccccccvcxvxcccccccccccccccccvcxvxcccccccccccccccccvcxvxcccccccccccccccccvcxvxcccccccccccccccccvcxvxcccccccccccccccccvcxvxcccccccccccccccccvcxvxcccccccccccccccccvcxvxccccccccccccccccc'),
(5, 'nico', 'bvbv', 'blabla@hot.com', 'bvbvbvv'),
(6, 'nico', 'no', 'blabla@hot.com', '44444444'),
(7, 'nico', 'no', 'blabla@hot.com', '44444444'),
(8, 'nico', 'no', 'blabla@hot.com', '65+65'),
(9, 'nico', 'no', 'blabla@hot.com', '65+65'),
(10, 'ds', 'dsds', 'blabla@hot.com', 'dsds'),
(11, 'ds', 'dsds', 'blabla@hot.com', 'dsds'),
(12, 'ds', 'dsds', 'blabla@hot.com', 'dsds');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id` int(11) UNSIGNED NOT NULL,
  `nom` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `prenom` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `pass` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `reset_token` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reset_at` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `user`
--

INSERT INTO `user` (`id`, `nom`, `prenom`, `email`, `pass`, `reset_token`, `reset_at`) VALUES
(4, 'Escroc', 'nicolas', 'escroc@fake.com', '$2y$10$4XEwbjgOXjPc7UReE5qYBeUgXSTCKmgmrUtH1QhTf7P4liEDu1F6K', NULL, NULL),
(6, 'nico', 'nico', 'nico@gmail.com', '$2y$10$lC2yIjc0TySfcCRXIrj9I.XihcW/l3gMp.IOzxC7NtatybAp/UcCS', NULL, '2018-12-12 10:23:12'),
(29, 'boiteEncarton2', 'nicolas', 'boite@gmail.com', '$2y$10$leIE1smD9pd8VxlLC1GlgeHnxtQh8eZB3d7jYS14SVdX3qwG4gynu', NULL, '2018-12-18 09:25:00');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `auth`
--
ALTER TABLE `auth`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `informations`
--
ALTER TABLE `informations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_auteur` (`id_auteur`);

--
-- Index pour la table `markers`
--
ALTER TABLE `markers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`);

--
-- Index pour la table `messagerie`
--
ALTER TABLE `messagerie`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `auth`
--
ALTER TABLE `auth`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `informations`
--
ALTER TABLE `informations`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT pour la table `markers`
--
ALTER TABLE `markers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT pour la table `messagerie`
--
ALTER TABLE `messagerie`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
